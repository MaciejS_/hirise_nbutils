import torch

def value_bounds_from_bands(bands, smap):
    abv = [torch.masked_select(smap, band.to(torch.bool)) for band in bands]
    value_domain_bounds = torch.stack([torch.as_tensor([bv.min(), bv.max()]) for bv in abv])
    return value_domain_bounds


def area_bounds_from_bands(bands, smap):
    band_areas = bands.reshape(bands.shape[0], -1).sum(dim=-1) / (smap.shape[-1] ** 2)
    area_bound_edges = torch.cumsum(torch.cat((torch.as_tensor([0.0]), band_areas), dim=0), dim=0)
    area_domain_bounds = torch.stack((area_bound_edges[:-1], area_bound_edges[1:])).T
    return area_domain_bounds


def energy_bounds_from_bands(bands, smap):
    band_energy = torch.cat([torch.masked_select(smap, band.to(torch.bool)).sum() for band in bands],
                            dim=0) / smap.sum()
    energy_bound_edges = torch.cumsum(torch.cat((torch.as_tensor([0.0]), band_energy), dim=0), dim=0)
    energy_domain_bounds = torch.stack((energy_bound_edges[:-1], energy_bound_edges[1:])).T
    return energy_domain_bounds
