import pathlib
import typing as t
import itertools as it
import more_itertools as mit
import os
import h5py

from functools import partial, reduce
import operator as op

import numpy as np
import pandas as pd

# visualization
from hirise.structures import *
from hirise.evaluation.utils import auc as calc_auc
from hirise_tools.annotation.pascal_voc import load_to_dataframes
from hirise_tools.annotation.utils import scale_bboxes, add_bbox_arrays
from hirise_tools.iterative.persistence import apply_to_leaves
from hirise_tools.utils import walk_files, randhex
from hirise_tools.voronoi.hdf import extract_dimension_scales
from hirise_tools.voronoi.params import extract_fixed_params_from_dataset
from hirise_tools.voronoi.paramsets import sanitize_bytestrings, split_up_coupled_params as split_couplings
from hirise_nbutils.imagenet_labels import idx_for_synset
from hirise_tools.voronoi.merge import merge_datasets_virtual, merge_scales

GAME_MAPPING = {
    'blur': 'destructive',
    'sharpen': 'constructive',
    'remove': 'destructive',
    'insert': 'constructive'
}

INVERSE_GAME_MAPPING = {
    'constructive': ['insert', 'sharpen'],
    'destructive': ['remove', 'blur'],
}

def create_idx_tuple(scales, requested_params):
    idx_tuple_as_list = []

    # parameters whose queries matched some values but not all (HDF only allows one such dimension index per indexoint tuple)
    # e.g.
    # * (:, 1, 1, :) is fine,
    # * (:, 1, [1,2], :) is fine,
    # * (:, [1, 3], [1, 2], :) will raise an exception in h5py
    _list_index_params = []
    for param_name, values in scales.items():
        if param_name in requested_params:
            param_query = requested_params.get(param_name)
            if callable(param_query):
                condition = param_query
            elif isinstance(param_query, float):
                condition = partial(math.isclose, param_query)
            elif isinstance(param_query, str):
                condition = partial(op.eq, param_query.encode('utf-8'))
            elif isinstance(param_query, np.ndarray):
                if param_query.dtype == object:
                    condition = lambda x: np.all(x == param_query)
                else:
                    condition = partial(np.allclose, param_query)
            else:
                condition = partial(op.eq, param_query)

            # region [append the index or raise if it's a second]

            idx_for_matching = list(i for i, v in enumerate(values) if condition(v))
            if len(idx_for_matching) == 1:
                idx_for_matching = mit.only(idx_for_matching)
            else:
                if not _list_index_params:
                    _list_index_params = [param_name]
                else:
                    raise ValueError(
                        f"Only one param can use advanced indexing (multi-idx params: {_list_index_params}, {param_name})"
                    )
        else:
            idx_for_matching = slice(None, None)

        idx_tuple_as_list.append(idx_for_matching)

    return tuple(idx_tuple_as_list)


# convert the ndarray to dataframe

def split_up_coupled_params(
    paramset: t.Dict[str, t.Any],
) -> t.Dict[str, t.Any]:
    """
    This is a "functional" variant of the original, which acts on individual paramsets, rather than entire collection,
    making it more suitable for use in
    """
    params_to_split = tuple(filter(lambda param_name: '+' in param_name, paramset))
    for coupled_param_name in params_to_split:
        individual_param_names = coupled_param_name.split('+')

        coupled_values = paramset.pop(coupled_param_name)
        paramset.update(zip(individual_param_names, coupled_values))
    return paramset


def convert_cls_to_int(paramset: t.Dict[str, t.Any]) -> t.Dict[str, t.Any]:
    paramset['cls'] = int(paramset['cls'])
    return paramset


def truncate_paths(paramset: t.Dict[str, t.Any]) -> t.Dict[str, t.Any]:
    paramset['img'] = os.path.basename(paramset['img'])
    return paramset


def convert_bytes_to_str(paramset: t.Dict[str, t.Any]) -> t.Dict[str, t.Any]:
    return {
        k: bytes.decode(v) if isinstance(v, bytes) else v
        for k, v
        in paramset.items()
    }


def ndarray_to_df(ndarray: np.ndarray, params: t.Dict[str, t.Collection[t.Any]]) -> pd.DataFrame:
    """
    Builds a pandas DataFrame out of provided `ndarray` and `params`.
    `params` has to be a dict containing mappings of parameter names to collection of values for that parameter.
    `params` are expected to be in the same order as dimensions of the `ndarray`.
    """
    param_order = params.keys()  # since analysis is ran on python3.7+, I can assume that the dict is ordered
    indices = {
        k: tuple(range(len(v)))
        for k, v
        in params.items()
    }

    paramsets_as_values = list(tuple(it.product(*params.values())))
    indexing_tuples = list(tuple(it.product(*indices.values())))

    paramsets = (
        {
            'auc': calc_auc(ndarray[idxt]),
            **{
                param_name: param_value
                for param_name, param_value
                in zip(param_order, _paramset_values)
            },
        }
        for _paramset_values, idxt
        in zip(paramsets_as_values, indexing_tuples)
    )

    sanitization_pipeline = (
        split_up_coupled_params,
        convert_cls_to_int,
        convert_bytes_to_str,
        truncate_paths,
    )

    records = (
        reduce(lambda x, f: f(x), sanitization_pipeline, record)
        for record
        in paramsets
    )

    split_coupled_names = partial(str.split, sep='+')
    result = pd.DataFrame.from_records(
        data=tuple(records),
        columns=list(it.chain.from_iterable(map(split_coupled_names, param_order))) + ['auc']
    )

    return result


def group_games_index(df):
    return df.reset_index(
        level='game'
    ).replace(
        {'game': GAME_MAPPING}
    )


group_games = group_games_index

def find_best_of_game(df_row: pd.Series) -> pd.Series:
    """
    Marks the best score with 'True' and others with False.
    Expects the name of the game to be present in name of the series.
    """
    index = set(df_row.name)
    if {"blur", 'remove'} & index:
        return df_row == np.nanmin(df_row)
    elif {"sharpen", "insert"} & index:
        return df_row == np.nanmax(df_row)
    else:
        return df_row

def find_best(df_row: pd.Series) -> pd.Series:
    """
    Convenience version of _find_best_of_game, which works for both scores and standard deviations.
    Marks the best score with 'True' and others with False.
    """
    index = set(df_row.name)
    if "std" in index:
        return df_row == np.nanmin(df_row)
    elif "mean" in index:
        return find_best_of_game(df_row)
    return df_row


def highlight_max(s: pd.Series, props=''):
    return np.where(s == np.nanmax(s.values), props, '')

def highlight_min(s: pd.Series, props=''):
    return np.where(s == np.nanmin(s.values), props, '')

def categorize_game(df: pd.DataFrame):
    """
    Uses information found ing the 'game' column to create two additional columns with category labels for each game entry.
    The inserted categories are: destructive/constructive ("category") and blurred/empty ("substrate")
    """
    return (
        df
        .assign(
            game_category=lambda df: df['game'].copy().replace({
                "blur": "destructive",
                "remove": "destructive",
                "insert": "constructive",
                "sharpen": "constructive",
            }),
            game_substrate=lambda df: df['game'].copy().replace({
                "blur": "blurred",
                "remove": "empty",
                "insert": "empty",
                "sharpen": "blurred",
            })
        )
    )


def reconstruct_paramsets(
    dataset: h5py.Dataset
) -> t.Tuple[
    t.Sequence[t.Dict[str, t.Any]],
    t.Sequence[t.Tuple[t.Union[int, t.Sequence[int], slice]]]
]:
    """
    THIS IS A (STRIPPED DOWN) COPY-PASTE OF THE `hirise_tools.voronoi.paramsets.prepare_paramsets`
    The reason why it's been copied is that the original version can't be used as-is and there isn't enough time to perform
    a refactor and modularize the original version and cover it with tests. `prepare_paramsets` is at the heart of
    all runner code and any regressions would be catastrophic.

    This function doesn't perform any parameter optimizations, just returns all combinations to aid
    in indexing HDF5 result files.

    TODO: Refactor `hirise_tools.voronoi.paramsets.prepare_paramsets`, so that this function right here could be removed
     entirely

    Returns a tuple containing:
        1. A list of full parameter set dicts (includes all types of params, from cartesian to fixed) extracted from
            scales and attrs of the `dataset`
        2. A list of indexing tuples for `dataset`, which are meant to be used when storing results of evaluation of
            the corresponding paramset. `dataset[idx_tuple] = results` guarantees correct placement of results
    """

    scale_params = extract_dimension_scales(dataset)
    scale_order = tuple(scale_params.keys())

    indices = {
        dim.label: tuple(range(dim_len))
        for dim, dim_len, dim_maxlen
        in zip(dataset.dims, dataset.shape, dataset.maxshape)
        if dim_maxlen is None
    }

    # NOTE: I don't need to separate ranged params from cartesian - all that matters is that the number of combinations
    #       is correct and
    #       If there's a param irrelevant to the evaluator, like run_idx, the evaluator can simply not read it from the
    #       paramset and ignore it. It will carry out the correct number of runs, because that exact paramset will appear
    #       N times on the list, anyway

    # step 1 - optimize the params
    # a parameter optimization can apply any transformation to any single parameter if the following rules are kept:
    # 1. an optimization of parameter A must not affect parameter B (unless they are coupled)
    # 2. the transformation applied to the list of parameter values (usually grouping) needs to be applied in the same
    #    way to the indexing tuples for this parameter (this ensures that results will be stores at correct locations
    #    in the result archive)

    #     # step 1.1 - apply parameter optimizations (hard-coded for now)
    #     # step 1.1.1 - group all classes evaluated on given img
    #     group_classes_by_img(scale_params, indices)

    #     # step 1.1.2 - wrap the list of 'chk' values in a single list (to make it appear as a single value to itertools.product)
    #     #  and replace "chk" with slice(None, None) in indexing tuples to mirror the change applied to the list of paramsets
    #     #   why slice() instead of just [1]? Because the state of each checkpoint needs to be stored separately
    #     #   so one parameter set evaluation will generate N maps for N checkpoints
    #     #   prior to this optimization, we'd get N evaluations, each generating one map for each of N checkpoints
    #     collapse_checkpoints(scale_params, indices)

    # step 1.2
    # convert bytestring values to strings to prevent false negatives when param values are compared to string literals
    scale_params = {
        # k: list(map(recursive_decode, values))
        k: apply_to_leaves(sanitize_bytestrings, values)
        for k, values
        in scale_params.items()
    }

    # region step 2 - convert paramset as tuples to paramsets as dicts (to make it easier for the evaluator to select the correct values)
    paramsets_as_values = list(tuple(it.product(*scale_params.values())))
    indexing_tuples = list(tuple(it.product(*indices.values())))

    paramsets = [
        {
            param_name: param_value
            for param_name, param_value
            in zip(scale_order, _paramset_values)
        }
        for _paramset_values
        in paramsets_as_values
    ]
    # endregion step 2

    # region step 3 - split up the combined params within each paramset
    # this step doesn't change the number of paramsets, only rearranges their contents to a form easier to digest
    # by the evaluator. It is carried out after the cartesian product because otherwise coupled params would be treated
    # like regular cartesian params
    # the indexing tuples don't need to be changed because the values of parameters in each parameter set don't change
    split_couplings(paramsets, scale_order) # this is an alias of `hirise_tools.voronoi.paramsets.split_up_coupled_params`
    # region step 3

    # region step 4 - include fixed params in paramsets (fixed params are retrieved from .attrs)
    fixed_params = extract_fixed_params_from_dataset(dataset)
    fixed_params.pop('runs')
    for paramset in paramsets:
        paramset.update(fixed_params)
    # region step 4

    return paramsets, indexing_tuples


def build_hdf_index(dataset: h5py.Dataset):
    paramsets, idxtuples = reconstruct_paramsets(dataset)
    keys = list(paramsets[0].keys())

    return (
        pd.DataFrame
        .from_records(paramsets)
        .assign(idxt=idxtuples)
        .set_index(keys)
    )


def load_imgnet_bboxes(xml_annotations_dir: str, scale_to_shape: t.Union[None, t.Tuple[int, int]] = None):
    """
    Return a dataframe containing ['img', 'cls', 'synset', 'bbox', 'xmin', 'xmax', 'ymin', 'ymax'] columns
    * 'bbox' contains an ndarray in shape expected by pointing_game: [2 (y.x), 2 (min.max)]

    if `scale_to_shape` is provided, 'bbox' values as well as the coord columns will be scaled to the shape provided as this function's arg (ZYX dim order is expected)
    """
    annot_xmls_iter = map(
        lambda path: pathlib.Path(path).read_text(),
        walk_files(xml_annotations_dir)
    )
    img_df, bbox_df = load_to_dataframes(annot_xmls_iter)

    if scale_to_shape is not None:
        bbox_df = scale_bboxes(bbox_df, img_df, scale_to_shape)


    bbox_df = add_bbox_arrays(bbox_df)
    bbox_df['cls'] = [idx_for_synset.get(synset, None) for synset in bbox_df['synset']]

    return bbox_df


def make_bbox_index(hdf_index: pd.DataFrame, bbox_df: pd.DataFrame) -> pd.DataFrame:
    """
    Merges HDF archive index (built by `make_hdf_index`) with a bbox dataframe.

    """
    return (
        hdf_index
        .reset_index()
        .assign(
            cls=lambda df: list(map(str, df['cls'])), # cast int column to str to allow merge on this column
            img=lambda df: [os.path.splitext(os.path.basename(x))[0] for x in df['img']],
            file=lambda df: df['img']
        )
        .merge(
            bbox_df.assign(cls=lambda df: list(map(str, df['cls']))),
            how="left",
            on=["img", "cls"],
        )
        .assign(
            cls=lambda df: list(map(int, df['cls'])), # cast cls back to int
        )
        # now, combine bbox instances for the same paramset (img+cls+others) into 3D ndarrays
        .groupby(
            list(set(hdf_index.columns) - {'bbox', 'idxt'})
        )
        .agg({
            'idxt': lambda x: mit.only(set(x)),
            'bbox': np.stack
        })
        .reset_index()
    )


def merge_parts_virtual(
    partfile_paths: t.Sequence[str],
    merged_ds_path: str,
    merged_dim: str,
    virtual_ds_path: str = "/tmp/"
) -> t.Tuple[h5py.File, h5py.Dataset]:
    """
    Merges part archives along `merged_dim` inside a newly created HDF5 virtual dataset
    Scale for `merged_dim` is merged as well (although by cloning, not via virtual dataset creation(
    Other scales are copied over as well.
    """

    if os.path.isdir(virtual_ds_path):
        vds_path = os.path.join(virtual_ds_path, f"{randhex()}.vh5")
    else:
        vds_path = virtual_ds_path

    vhdf_file = h5py.File(vds_path, 'w', libver="latest")

    # full_ds_path = mit.only({ds.name for ds in part_datasets})
    ds_group_path, ds_name = os.path.split(merged_ds_path)

    part_files = [h5py.File(path, 'r', swmr=True) for path in partfile_paths]
    part_datasets = [file[merged_ds_path] for file in part_files]

    # recreate the group hierarchy
    base_group = vhdf_file
    for groupname in ds_group_path.strip('/').split('/'):
        base_group = base_group.require_group(groupname)
    scales_group = base_group.require_group('_scales')

    merge_scales(scales_group, merged_dim, [pds.parent['_scales'] for pds in part_datasets])
    merge_datasets_virtual(base_group, ds_name, merged_dim, part_datasets)

    # close the files before
    for file in part_files:
        file.close()
    vhdf_file.close()

    # reopen the virtual DS in read-only mode (to prevent soft file corruption when kernel crashes and the write flag isn't removed)
    vhdf_file = h5py.File(vds_path, 'r', swmr=True, libver="latest")

    return vhdf_file, vhdf_file[merged_ds_path]