import numpy as np
import scipy.stats
import skimage.feature

from hirise.experiment.utils import spearman_upper_matrix, confusion_triu
from skimage.metrics import structural_similarity as ssim

def relative_diff(x, reference):
    return (x - reference) / reference

def adjusted_diff(x, reference):
    """
    The improvement metric. Yields fractions of possible improvement headroom (or possible deterioration) taken advantage of by `x
    This metric works only for values in range [0, 1] and only for maximizing metrics.
    If evaluated metric is minimizing (lower == better), then its results must be horizontally flipped beforehand

    Examples:
    # because (0.85-0.75)==0.1, which is 20% of total room for improvement: (1-0.75)=0.25
    >>> np.allclose(adjusted_diff(0.85, 0.75), 0.4)
    True

    # because (0.6-0.75)==0.15, which is 20% of total possible deterioration: 0.75
    >>> np.allclose(adjusted_diff(0.6, 0.75), -0.2)
    True

    >>> np.allclose(adjusted_diff(1, 0.75), 1) # because the metric has improved as much as possible
    True

    >>> np.allclose(adjusted_diff(0, 0.75), -1) # because the metric has deteriorated as much as possible
    True
    """
    denominator = np.where(
        x > reference,
        np.maximum(1 - reference, np.finfo(float).eps),
        np.maximum(reference, np.finfo(float).eps)
    )
    return (x - reference) / denominator


def spearman_rank(x, y):
    correlation, pvalue = scipy.stats.spearmanr(x, y, axis=None)
    return correlation

def mse2(x,y):
    return np.mean((x-y)**2, axis=None)

def hog(smap):    
    return skimage.feature.hog(smap, 
                               orientations=8, 
                               pixels_per_cell=(16, 16), 
                               cells_per_block=(1, 1), 
                               visualize=False, 
                               multichannel=False, 
                               feature_vector=True)

def pearson(x,y):
    try:
        correlation, pvalue = scipy.stats.pearsonr(x.flatten(), y.flatten())
        return correlation
    except Exception as e:
        print(x.shape, y.shape)
        raise e


def spearman_triu(maps): return confusion_triu(maps, spearman_rank, op_dims=2)
def ssim_triu(maps): return confusion_triu(maps, lambda x,y: ssim(x, y, window_size=5), op_dims=2)
def mse2_triu(maps): return confusion_triu(maps, mse2, op_dims=2)
def pearson_on_hog_triu(maps): 
    original_shape = maps.shape
    flattened_maps_tensor = maps.reshape(-1, *original_shape[-2:])
    flattened_hogs = np.array(list(map(hog, flattened_maps_tensor))).reshape(*original_shape[:-2], -1)
    return confusion_triu(flattened_hogs, pearson, op_dims=1)


similarity_methods = {"spearman": {"lambda": spearman_triu,
                        "desc": "higher is better",
                        "ylabel": "Spearman Rank value"},
           "ssim": {"lambda": ssim_triu,
                   "desc": "higher is better",
                   "ylabel": "SSIM score"},
           "mse": {"lambda": mse2_triu,
                   "desc": "lower is better",
                   "ylabel": "MSE value"},
           "pearson_of_hog": {"lambda": pearson_on_hog_triu,
                               "desc": "higher is better",
                               "ylabel": "Pearson correlation of SMap HoGs"}}


similarity_to_noise_methods = {"spearman": spearman_rank,
                 "ssim": lambda x, y: ssim(x, y, window_size=5),
                 "mse": mse2,
                 "pearson_of_hog": lambda x, y: pearson(hog(x), hog(y))}