import math
import numpy as np
import torch

from hirise.experiment.utils import normalize, torch_normalize
from hirise.utils import make_displayable
from hirise_nbutils.utils import reorder

from hirise.evaluation.utils import auc

import viz


from matplotlib import pyplot as plt
import matplotlib.gridspec as gridspec



def viz_compare_hirise_runs_vs_reference(fig, gs_slot, img, n_evals_shape, combined_smaps, reference_maps, hirise_aucs, reference_aucs, s_values, N_values, observed_class):
    reference_maps = [reference_maps] if not isinstance(reference_maps, (list, tuple)) else reference_maps
    total_N = N_values.sum()
    disp_img = make_displayable(img.cpu())

    axes = []
    
    n_rows = len(reference_maps)
#     fig = plt.figure(figsize=(n_rows * 6, n_rows * 1.5), constrained_layout=True, facecolor="white")
    gs = gs_slot.subgridspec(1, 3)
    gs.wspace = 0.2

    auc_argsort = np.argsort(hirise_aucs)
    mean_ref_auc = np.mean(reference_aucs)
    
    # visualization of hirise masks
    # masks are sorted by score
    sgs = gs[0].subgridspec(*n_evals_shape)
    subaxes = []
    for idx, (y, x) in enumerate(np.ndindex(*n_evals_shape)):
        ax = fig.add_subplot(sgs[y, x])
        subaxes.append(ax)
        mask_index = auc_argsort[idx]
        viz.imshow(ax, combined_smaps[mask_index])
        ax.imshow(disp_img, alpha=0.15)
        ax.set_ylabel(f"Mask {mask_index}\nAuC: {hirise_aucs[mask_index]:.3f}")
        if y == 0 and x == int(n_evals_shape[1] / 2):
            ax.set_title(f"HiRISE maps: s: {'->'.join(map(str, s_values))}, N: {','.join(map(str, N_values))}")
    axes.append(subaxes)
            
    # visualization of one of the reference maps
    ax = fig.add_subplot(gs[1])
    viz.imshow(ax, reference_maps[0])
    ax.imshow(disp_img, alpha=0.15)
    ax.set_title(f"Reference RISE map - {np.sum(N_values)} masks at s={max(s_values)}")
    ax.set_xlabel(f"AuC: {mean_ref_auc:.3f}")
    axes.append(ax)
    
    # AuC scores for deletion game
    ax = viz.viz_aucs_barplot(fig, gs[2], 
                         ref_aucs=reference_aucs, 
                         hirise_aucs=hirise_aucs)
    axes.append(ax)
    return axes
    # Rank viz (per band)
    # ax = fig.add_subplot(gs[4])
#     viz.viz_rank_for_multiple_hirise_runs()



def viz_hirise_iteration_multiinstance(fig, gs_slot, img, reference_smaps, reference_scores, hirise_smaps, scores,
                                       prev_iteration_maps, bounds, bands, prev_bands, hirise_del_scores, ref_del_scores, N_values,
                                       iteration_idx, observed_class, figsize=None):
    subplotshape = (math.ceil(math.sqrt(len(hirise_smaps))),) * 2
    disp_img = make_displayable(img.cpu())
    is_first_iteration = prev_iteration_maps is None

    # sort AuC scores
    hirise_aucs, ref_aucs = [list(map(auc, rep_scores))
                         for rep_scores 
                         in (hirise_del_scores, ref_del_scores)]
    
    auc_argsort = np.argsort(hirise_aucs)
    mean_ref_auc = np.mean(ref_aucs)

    gs = gs_slot.subgridspec(1, 5)

    # viz bands and bounds
    if is_first_iteration:
        ax = fig.add_subplot(gs[0])
        ax.axis('off')
        ax.set_title("Bounds based on previous maps")
    else:
        viz.viz_bands_for_multiple_maps(fig, gs[0], 
                                        plotshape=subplotshape, 
                                        disp_img=disp_img, 
                                        saliency_bands=bands)

    # viz smaps
    #     title = f"HiRISE maps: s: {'->'.join(map(str, s_values))}, N: {','.join(map(str, N_values))}"
    map_axs = viz.viz_maps(fig, gs[1], 
                           disp_img=disp_img, 
                           maps=hirise_smaps, 
                           n_evals_shape=subplotshape, 
                           title="")
    sorted_aucs = reorder(hirise_aucs, auc_argsort)
    for idx, ax in enumerate(map_axs):
        ax.set_ylabel(f"Mask {idx}\nAuC: {sorted_aucs[idx]:.3f}")

    # viz reference map
    ax = fig.add_subplot(gs[2])
    viz.imshow(ax, reference_smaps[0])
    ax.imshow(disp_img, alpha=0.15)
    ax.set_title(f"Basic RISE: N={np.sum(N_values[:iteration_idx + 1])} s={bands[iteration_idx][0].shape[-1]}")
    ax.set_xlabel(f"AuC: {mean_ref_auc:.3f}")

    # viz AuCs
    ax = viz.viz_aucs_barplot(fig, gs[3], 
                              ref_aucs=ref_aucs, 
                              hirise_aucs=hirise_aucs)
    ax.set_title("Deletion game score")

    # viz deletion game curves
    ax = fig.add_subplot(gs[4])
    if is_first_iteration:
        ax.set_title("Deletion game progress")
    viz.viz_del_score_curves(ax, hirise_del_scores, ref_del_scores)
    
#     # viz ranks
#     ax = fig.add_subplot(gs[4])
#     #     if is_first_iteration:
#     #         ax.axis('off')
#     #     else:
#     ax = viz.viz_rank_for_multiple_hirise_runs(ax, 
#                                                banded_scores_for_runs=scores, 
#                                                ref_scores=reference_scores, 
#                                                ranker_lambda=ranks.from_avg_score, 
#                                                observed_class=observed_class)
#     ax.set_title("Avg in-band rank")


# out-of-axes annotation example  (just need to figure out how to prevent it from shrinking adjacent lots)
#         if idx == 0 and is_first_iteration:
#             ax.annotate('...Additional information...',
#                         xy=(0, 0.9), xytext=(0, 20),
#                         xycoords=('axes fraction', 'figure fraction'),
#                         textcoords='offset points',
#                         size=14, ha='center', va='top')


def viz_multiteration_hirise(img, smaps, all_scores, all_bounds, all_bands, all_reference_smaps, ref_scores, all_del_scores,
                             ref_del_scores, N_values, observed_class, figsize=None):
    n_rows = len(smaps)

    viz_scaling_factor = 1
    figsize = figsize or [4 * 6, n_rows * 3 + 2]
    fig = plt.figure(figsize=figsize, constrained_layout=True, facecolor="white")

    gs = gridspec.GridSpec(n_rows + 1, 1, fig)
    gs.wspace = 0.2

    for idx in range(n_rows):
        prev_smaps = smaps[idx - 1] if idx > 0 else None
        prev_bands = all_bands[idx - 1] if idx > 0 else None
        viz_hirise_iteration_multiinstance(fig=fig,
                                           gs_slot=gs[idx],
                                           img=img,
                                           reference_smaps=all_reference_smaps[idx],
                                           reference_scores=ref_scores[idx],
                                           hirise_smaps=smaps[idx],
                                           scores=all_scores[idx],
                                           prev_iteration_maps=prev_smaps,
                                           bounds=all_bounds[idx],
                                           bands=all_bands[idx],
                                           prev_bands=prev_bands,
                                           hirise_del_scores=all_del_scores[idx],
                                           ref_del_scores=ref_del_scores[idx],
                                           N_values=N_values,
                                           iteration_idx=idx,
                                           observed_class=observed_class)



    

def banded_masks_visualization(img, bands, masks, partial_smaps, bounds, bound_edges, hirise_del_scores, ref_del_scores, band_gen, bound_gen, n_mask_examples=3, cmap=None, figsize=None):
    fig = plt.figure(figsize=figsize or (n_mask_examples * 8, n_mask_examples + 2), constrained_layout=True, facecolor="white")
    # fig.suptitle(f"Examples of grids at $s=${s} and $p1=${p1}\n (ceil($p_1s^2$) = {int(p1 * s**2)} cells should be picked on each grid)")
    n_bands = len(bands)
    disp_img = make_displayable(img.cpu().numpy() if isinstance(img, torch.Tensor) else img)
    
    gs = gridspec.GridSpec(1, 1, fig)
    gs.wspace = 0.2

    viz.viz_masks_and_bands(fig, gs[0], 
                            disp_img=disp_img, 
                            bands=bands, 
                            masks=masks, 
                            partial_smaps=partial_smaps, 
                            prev_smap=torch_normalize(partial_smaps.sum(dim=0)),
                            bounds=bounds, 
                            bound_edges=bound_edges, 
                            hirise_del_scores=hirise_del_scores[idx],
                            ref_del_scores=ref_del_scores[idx],
                            band_gen=band_gen, 
                            bound_gen=bound_gen, 
                            n_mask_examples=n_mask_examples, 
                            cmap=cmap)


    
def multiiteration_banded_masks_visualization(img, all_bands, all_sample_masks, all_banded_smaps, all_bounds, bound_edges, hirise_del_scores, ref_del_scores, band_gen, bound_gen, n_mask_examples=3, cmap=None, figsize=None):
    n_rows = len(all_sample_masks)
    n_bands = max(len(iteration_bands) for iteration_bands in all_bands)
    
    fig = plt.figure(figsize=figsize or (n_mask_examples * 12, n_rows * (n_bands + 2)), constrained_layout=True, facecolor="white")
    # fig.suptitle(f"Examples of grids at $s=${s} and $p1=${p1}\n (ceil($p_1s^2$) = {int(p1 * s**2)} cells should be picked on each grid)")
    disp_img = make_displayable(img.cpu().numpy() if isinstance(img, torch.Tensor) else img)
    
    gs = gridspec.GridSpec(n_rows, 1, fig)
    gs.wspace = 0.2
    
    print(f"Using {bound_gen.__name__}")
    for idx, gs_slot in enumerate(gs):
        prev_smap = torch_normalize(all_banded_smaps[idx-1].sum(dim=0)) if idx > 0 else None
        viz.viz_masks_and_bands(fig, gs_slot, 
                                disp_img=disp_img, 
                                bands=all_bands[idx], 
                                masks=all_sample_masks[idx],
                                partial_smaps=all_banded_smaps[idx],  
                                prev_smap=prev_smap,
                                bounds=all_bounds[idx],
                                hirise_del_scores=hirise_del_scores[idx],
                                ref_del_scores=ref_del_scores[idx],
                                bound_edges=bound_edges,
                                band_gen=band_gen, 
                                bound_gen=bound_gen,
                                n_mask_examples=n_mask_examples, 
                                iteration_idx=idx)
        
# def bands_curves_and_bars