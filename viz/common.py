from typing import Union, Tuple

import matplotlib
import numpy as np
import torch
import torch.nn.functional as tnnf

from hirise.experiment.utils import normalize, torch_normalize
from hirise.occlusions.distributions_torch import invert, make_dfn, make_cdfn, make_interpolating_callable as interpolating_callable
from hirise.occlusions.bands import SaliencyValueThresholdGen, PercentileThresholdGen, EnergyThresholdGen
from hirise_nbutils.bands import area_bounds_from_bands

from matplotlib import patches
from matplotlib import pyplot as plt
from matplotlib.colors import Colormap
import matplotlib.colors as mcolors


def ref_and_samples_color_scheme(n_samples, ref_color="orange", sample_color="blue"):
    return [mcolors.CSS4_COLORS[ref_color]] + [mcolors.CSS4_COLORS[sample_color]] * n_samples


def color_scale(n, cmap, desc=True, vmin=0, vmax=1):
    sampling_points = normalize((np.arange(n) + 1) / n, original_range=(0, 1), new_range=(vmin, vmax))
    _cmap = cmap if isinstance(cmap, Colormap) else plt.get_cmap(cmap)
    colors = list(map(_cmap, sampling_points))

    return colors if not desc else list(reversed(colors)) 

def cmap_add_alpha(cmap):
    """
    Blends in a linear alpha channel to `cmap`, from fully transparent at cmap(0) to fully opaque at cmap(1)
    Courtesy of: https://stackoverflow.com/a/37334212/11048027
    """
    my_cmap = cmap(np.arange(cmap.N))
    my_cmap[:, -1] = np.linspace(0, 1, cmap.N)
    my_cmap = mcolors.ListedColormap(my_cmap)
    return my_cmap


def subplots(plotshape, figsize=None, figscale=(4,4), **kwargs):
    return plt.subplots(
        *plotshape,
        figsize=figsize or list(reversed([x*s for x, s in zip(plotshape, figscale)])),
        facecolor="white",
        **kwargs
    )


def hide_ticks(ax):
    ax.set_xticks([], minor=[])
    ax.set_yticks([], minor=[])


def hide_spines(ax):
    """
    Hides the plot frame (a.k.a. spines) but doesn't affect ticks or labels
    """
    for spine in ax.spines.values():
        _ = spine.set_visible(False)
    return ax
hide_plot_frame = hide_spines


def imshow(ax, im, vmin=None, vmax=None, cmap="viridis", **kwargs):
    ax.imshow(im, vmin=vmin, vmax=vmax, cmap=cmap, **kwargs)
    hide_ticks(ax)


def gridimshow(ax, im, vmin=None, vmax=None, cmap="viridis", **kwargs):
    imshow(ax, im, vmin, vmax, cmap, **kwargs)
    #     ax.imshow(im, vmin=vmin, vmax=vmax, cmap=cmap)
    ax.grid(color='w', linewidth=1, linestyle='-')
    imsize = im.shape[0]
    ax.set_yticks(np.arange(-.5, imsize, 1), minor=False)
    ax.set_xticks(np.arange(-.5, imsize, 1), minor=False)
    ax.set_xticklabels([])
    ax.set_yticklabels([])


def zoom_out_and_center(ax: matplotlib.axes.Axes,
                        points: np.array,
                        center: Union[Tuple[float], np.array, None] = np.array([0.5, 0.5]),
                        zoom_out_ratio: float = 1.05,
                        keep_aspect_ratio: bool = False):
    """
    Adjusts the Y and X scale limits to fit all `points` in the plot and centers the visualization around `center` point.

    Adjust the x and y axis limits to keep all `points` visible
    :param ax: axes to draw on
    :param points: 2D array of yx coordinates of the points to factor in for in the visualization
    :param center: yx coordinates of the central point of the plot
    :param zoom_out_ratio: float: if 1, determines how much space (as a fraction of the distance from axis) will be left between furthest point and the edge of the plot
    :param keep_aspect_ratio: if True, the X:Y scale ratio will be equal to 1. Otherwise, each axis will be scaled separately
    :return:
    """
    center = np.array(center) if center is not None else np.array([0.0, 0.0])
    offsets_from_center = np.abs(-center + points, axis=0)

    if keep_aspect_ratio:
        d = zoom_out_ratio * np.max(offsets_from_center)
        ax.set_ylim(-d + center[0], d + center[0])
        ax.set_xlim(-d + center[1], d + center[1])
    else:
        dy, dx = zoom_out_ratio * np.max(offsets_from_center, axis=0)
        ax.set_ylim(-dy + center[0], dy + center[0])
        ax.set_xlim(-dx + center[1], dx + center[1])


def errplot(ax, y_series, candle_quantiles=(0.25, 0.75), outliers=True, colors=None, x_series=None, labels=None,
            alpha=1):
    """

    """
    if y_series.ndim != 2:
        raise NotImplementedError("only 2D y_series are supported")

    x_series = x_series if x_series is not None else np.arange(y_series.shape[0])

    colors = colors or [mcolors.CSS4_COLORS["blue"] for _ in x_series]
    labels = labels or [None for _ in x_series]
    if len(labels) < len(x_series):
        labels = labels + [None for _ in x_series[len(labels):]]

    if len(x_series) != len(y_series):
        raise ValueError(f"Mismatched x and y series lengths ({len(x_series)}, {len(y_series)})")

    for idx, (x, ys, color, label) in enumerate(zip(x_series, y_series, colors, labels)):
        y_median = np.array([np.median(ys)])
        low, high = map(lambda q: np.quantile(ys, q), candle_quantiles)

        ax.scatter(x, y_median, color=color, label=label, alpha=alpha)
        ax.errorbar(x, y_median, yerr=[y_median - low, high - y_median], capsize=5, color=color, alpha=alpha)

        if outliers:
            outlier_y = np.concatenate([ys[ys < low], ys[ys > high]])
            ax.scatter(np.full_like(outlier_y, x), outlier_y, color=color, alpha=0.5 * alpha, marker='.')

    ax.legend()


def dotplot(ax, data, *args, **kwargs):
    """
    A variation of heatmap plot, where each heatmap cell is a single datapoint of a scatter plot.
    The color intensity of the datapoint expresses its value.
    """
    if data.ndim != 2:
        raise ValueError("Expected 2D input")
    my, mx = map(np.ndarray.flatten, np.mgrid[:data.shape[0], :data.shape[1]])
    return ax.scatter(
        y=my,
        x=mx,
        c=data.flatten(),
        *args,
        **kwargs
    )


def get_linspace_transformation_for_bound_gen(bound_gen):
    if bound_gen == EnergyThresholdGen:
        icdfn = lambda x: interpolating_callable(invert(make_cdfn(x)))
        dfn = lambda x: interpolating_callable(make_dfn(x))
        return lambda smap: lambda x: dfn(smap)(icdfn(smap)(x))
    elif bound_gen == PercentileThresholdGen:
        return lambda x: interpolating_callable(make_dfn(x))
    elif bound_gen == SaliencyValueThresholdGen:
        return lambda x: interpolating_callable(torch.linspace(0, 1, len(x)))
    else:
        raise TypeError(f"Unknown BoundGen type: {type(bound_gen)}")


def get_inv_linspace_transformation_for_bound_gen(bound_gen):
    if bound_gen == EnergyThresholdGen:
        cdfn = lambda x: interpolating_callable(make_cdfn(x))
        idfn = lambda x: interpolating_callable(invert(make_dfn(x)))
        return lambda smap: lambda x: cdfn(smap)(idfn(smap)(x))
    elif bound_gen == PercentileThresholdGen:
        return lambda x: interpolating_callable(invert(make_dfn(x)))
    elif bound_gen == SaliencyValueThresholdGen:
        return lambda x: interpolating_callable(torch.linspace(0, 1, len(x)))
#         return lambda x: x
    else:
        raise TypeError(f"Unknown BoundGen type: {type(bound_gen)}")


def viz_band_area_barplot(ax, smap, bands, band_colors=None):
    if band_colors is None:
        band_colors = color_scale(len(bands), cmap="Blues", vmin=1/len(bands), desc=False)
   
    for idx, (band, color) in enumerate(zip(bands, band_colors)):
        area = band.sum() / torch.prod(torch.as_tensor(smap.shape))
        ax.bar(idx, area, color=color)
    
    ax.set_title("Band area")
    ax.set_ylim(0, 1)
    
def viz_band_value_barplot(ax, smap, bands, band_colors=None):
    if band_colors is None:
        band_colors = color_scale(len(bands), cmap="Blues", vmin=1/len(bands), desc=False)
   
    for idx, (band, color) in enumerate(zip(bands, band_colors)):
        masked_partial_smap = torch.masked_select(smap, band.to(torch.bool)) 
        try:
            value = masked_partial_smap.max() - masked_partial_smap.min()
        except RuntimeError:
            value = 0
        ax.bar(idx, value, color=color)
    
    ax.set_ylim(0, 1)
    ax.set_title("Band value delta")
    
def viz_band_energy_barplot(ax, smap, bands, band_colors=None):
    # plot band energy (bar)
    if band_colors is None:
        band_colors = color_scale(len(bands), cmap="Blues", vmin=1/len(bands), desc=False)
    
    partial_smaps = bands * smap
    for idx, (partial_smap, color) in enumerate(zip(partial_smaps, band_colors)):
        energy = partial_smap.flatten().sum() / smap.flatten().sum()
        ax.bar(idx, energy, color=color)
    
    ax.set_ylim(0, 1)
    ax.set_title("Band energy")
        

def viz_bands_on_df(ax, transformation, x_domain_bounds, y_domain_bounds=None):
    distrib_x = np.linspace(0, 1, 100)

    y = transformation(distrib_x)

    ax.scatter(distrib_x, y, marker='.')
    ax.set_ylim(0, 1)
    ax.set_xlim(0, 1)

    if y_domain_bounds is None:
        y_domain_bounds = transformation(x_domain_bounds)
    
    cmap = plt.get_cmap("Blues")
    for x, ((xlow, xhigh), (ylow, yhigh)) in enumerate(zip(x_domain_bounds, y_domain_bounds)):
        color_val = (x + 1) / len(y_domain_bounds)
        color = cmap(color_val)
        ax.axvspan(xmin=xlow, xmax=xhigh, ymax=yhigh, color=color, alpha=0.7, linewidth=0)
        ax.axhspan(ymin=ylow, ymax=yhigh, xmax=xlow, color=color, alpha=0.7, linewidth=0)
        ax.set_ylabel("saliency value")


def viz_bounds_on_map(ax, saliency_bounds, original_smap, disp_img, band_gen, bound_gen, cmap):
    cmap = cmap or plt.get_cmap("Blues")
    bands = band_gen.generate_bands(torch_normalize(original_smap), saliency_bounds, disp_img.shape[0])
    for i, band in enumerate(bands):
        color_val = (i + 1) / len(bands)
        imshow(ax, band * color_val, cmap=cmap, vmin=0, vmax=1, alpha=0.5)
    ax.imshow(disp_img, alpha=0.15)
    return ax


def viz_bounds_for_multiple_maps(fig, gridspec_slot, plotshape, bounds, original_smaps, disp_img, band_gen, bound_gen,
                                 cmap=None):
    sgs = gridspec_slot.subgridspec(*plotshape)
    axs = []
    for idx, (bound_values, previous_smap, (y, x)) in enumerate(zip(bounds, original_smaps, list(np.ndindex(*plotshape)))):
        ax = fig.add_subplot(sgs[y, x])
        axs.append(ax)
        viz_bounds_on_map(ax, 
                          saliency_bounds=bound_values, 
                          original_smap=previous_smap, 
                          disp_img=disp_img, 
                          band_gen=band_gen, 
                          bound_gen=bound_gen, 
                          cmap=cmap)

    return sgs


def viz_bands(ax, bands, disp_img, cmap):
    cmap = cmap or plt.get_cmap("Blues")
    target_shape = disp_img.shape[:2]  # [:2] is right - displayable image's shape is (y, x, RGB)
    upsampled_bands = tnnf.interpolate(bands.unsqueeze(1), target_shape, mode='nearest')
    for i, band in enumerate(upsampled_bands.squeeze(1)):
        color_val = (i + 1) / len(bands)
        color = cmap(color_val)
        imshow(ax, band * color_val, cmap="Blues", vmin=0, vmax=1, alpha=0.5)

    ax.imshow(disp_img, alpha=0.15)
    return ax


def viz_bands_for_multiple_maps(fig, gridspec_slot, plotshape, disp_img, saliency_bands, cmap=None):
    sgs = gridspec_slot.subgridspec(*plotshape)
    axs = []
    for idx, (bands, (y, x)) in enumerate(zip(saliency_bands, list(np.ndindex(*plotshape)))):
        ax = fig.add_subplot(sgs[y, x])
        axs.append(ax)
        viz_bands(ax, bands, disp_img, cmap)

    #         if y == 0 and x == int(plotshape[1]/2):
    #             ax.set_title(f"Used saliency bands")

    return axs


def calc_ranks_for_viz(all_banded_scores, ranker_lambda):
    return torch.stack(list(map(ranker_lambda, all_banded_scores)))


def viz_rank_per_band(ax, ranks, ref_rank, band_colors=None, ref_color=None):
    """
    Visualizes ranks for each band
    Ranks should be normalized to (0, 1) range
    """
    n_bands = ranks.shape[-1]

    ref_color = ref_color or 'Orange'
    default_cmap = plt.get_cmap("Blues")
    colors = band_colors or color_scale(len(ranks), default_cmap, vmax=0.6)

    ax.scatter(0, ref_rank, color=plt.get_cmap('Oranges')(0.3))
    ax.set_ylim(0, 1)
    for i, (rank, color) in enumerate(zip(ranks.numpy(), colors)):
        ax.scatter(i + 1, rank, color=color, alpha=0.5)


def viz_rank_for_single_hirise_run(ax, banded_scores, ref_scores, ranker_lambda, observed_class, band_colors=None):
    """
    Expected shapes:
    `ref_scores`: [bands, T(N_masks, n_classes)]
    `banded_scores`: [bands, T(N_masks, n_classes)]
    """

    ranks = calc_ranks_for_viz(banded_scores, ranker_lambda)
    ref_ranks = torch.stack(list(map(ranker_lambda, ref_scores))).mean(dim=0)
    n_classes = ranks.shape[-1]

    viz_rank_per_band(ax, 
                      ranks=ranks[:, observed_class] / n_classes, 
                      ref_rank=ref_ranks[observed_class] / n_classes, 
                      band_colors=band_colors)


def viz_rank_for_multiple_hirise_runs(ax, banded_scores_for_runs, ref_scores, ranker_lambda, observed_class,
                                      band_colors=None):
    """
    Expected shapes:
    `ref_scores`: [reps, bands, T(N_masks, n_classes)]
    `banded_scores`: [bands, T(N_masks, n_classes)]
    """
    cmaps = ["Blues", "Reds", "Greens", "Purples", "Greys"]
    band_colors = band_colors or [color_scale(len(bands), cmap) for bands, cmap in zip(banded_scores_for_runs, cmaps)]
    if len(banded_scores_for_runs) > len(cmaps):
        raise RuntimeError(
            f"You need to define additional colormaps to visualize ranks for more than {len(cmaps)} HiRISE runs. (Requested viz of {len(banded_scores_for_runs)} runs)")

    for idx, (run_banded_scores, colors) in enumerate(zip(banded_scores_for_runs, band_colors)):
        viz_rank_for_single_hirise_run(ax, 
                                       banded_scores=run_banded_scores, 
                                       ref_scores=ref_scores, 
                                       ranker_lambda=ranker_lambda, 
                                       observed_class=observed_class, 
                                       band_colors=colors)

    return ax


def viz_maps(fig, gs_slot, disp_img, maps, n_evals_shape, title):
    sgs = gs_slot.subgridspec(*n_evals_shape)
    axs = []
    for idx, (y, x) in enumerate(np.ndindex(*n_evals_shape)):
        ax = fig.add_subplot(sgs[y, x])
        imshow(ax, maps[idx])
        ax.imshow(disp_img, alpha=0.15)
        if y == 0 and x == int(n_evals_shape[1] / 2):
            ax.set_title(title)
        axs.append(ax)

    return axs


def viz_aucs_barplot(fig, gs_slot, ref_aucs, hirise_aucs, ylim=None, colors=None):
    colors = colors or ['blue', ] * len(hirise_aucs)
    ylim = ylim or (0, max(ref_aucs + hirise_aucs))

    mean_ref_auc = np.mean(ref_aucs)

    ax = fig.add_subplot(gs_slot)
    ax.bar(0, mean_ref_auc,
           label="avg rise",
           color='orange',
           alpha=0.7)

    for itx, (auc_value, color) in enumerate(zip(hirise_aucs, colors)):
        ax.bar(itx + 1, auc_value,
               label="hirise" if itx == 0 else None,
               color=color,
               alpha=0.7)

    ax.axhline(mean_ref_auc, color='r')
    ax.axhline(np.mean(hirise_aucs), color='b', label="avg hirise")
    ax.set_ylim(*ylim)
    ax.set_ylabel("AuC (lower is better)")
    ax.set_title("Deletion game result")
    ax.legend()
    return ax


def viz_banded_masks(fig, gs_slot, disp_img, bands, sample_masks, partial_smaps, n_mask_examples=3, cmap=None):
    n_bands = len(bands)
    sgs = gs_slot.subgridspec(n_bands, 1 + n_mask_examples + 1)

    target_shape = disp_img.shape[:2]
    bands = tnnf.interpolate(bands.unsqueeze(1), target_shape, mode='nearest').squeeze(1)
    for y in range(n_bands):
        # band subplot
        ax = fig.add_subplot(sgs[y, 0])
        if cmap is not None:
            imshow(ax, bands[y].cpu().squeeze(), vmin=0, vmax=1, cmap=cmap, alpha=(y + 1) / n_bands)
        else:
            imshow(ax, bands[y].cpu().squeeze(), vmin=0, vmax=1)
        ax.imshow(disp_img, alpha=0.15)
        ax.set_ylabel(f"band {y}")

    for y, x in np.ndindex(n_bands, n_mask_examples):
        ax = fig.add_subplot(sgs[y, x + 1])
        imshow(ax, sample_masks[y][x].cpu().numpy().squeeze())

    for y in range(n_bands):
        ax = fig.add_subplot(sgs[y, -1])
        imshow(ax, partial_smaps[y].cpu().squeeze())
        ax.imshow(disp_img, alpha=0.15)
        ax.set_title(f"partial smap {y}")


def viz_masks_and_bands(fig, gs_slot, 
                        disp_img, 
                        bands,
                        masks,
                        partial_smaps, 
                        prev_smap,
                        bounds,
                        bound_edges,
                        hirise_del_scores,
                        ref_del_scores,
                        band_gen,
                        bound_gen,
                        n_mask_examples=3, cmap=None, iteration_idx=None):
    sgs = gs_slot.subgridspec(1, 6)

    if iteration_idx is None or iteration_idx > 0:
        
        # calculate bands for viz and also as a base for are bounds inference (for bound->area viz on DF)
        native_bands = band_gen.generate_bands(prev_smap, bounds, prev_smap.shape[-1])
        area_domain_bounds = area_bounds_from_bands(native_bands, prev_smap)
#         m_sizes = torch.cumsum(native_bands.reshape(native_bands.shape[0], -1).sum(dim=-1), dim=0) / (disp_img.shape[0] ** 2 )
#         area_bound_edges = torch.cat((torch.as_tensor([0.0]), m_sizes), dim=0)
#         area_domain_bounds = torch.stack((area_bound_edges[:-1], area_bound_edges[1:])).T
        
        # viz of saliency bounds on map 
        ax = fig.add_subplot(sgs[0])
        ax.set_title("Saliency bands (native resolution)")
        ax.set_ylabel(f"Iteration {iteration_idx}" if iteration_idx else "")
        viz_bands(ax, native_bands, disp_img, cmap)
    
    
        # bound transformations viz
        input_to_saliency = get_linspace_transformation_for_bound_gen(bound_gen)(prev_smap.flatten())
        saliency_to_area = get_inv_linspace_transformation_for_bound_gen(PercentileThresholdGen)(prev_smap.flatten())
    
         
        # saliency bands visuzalized on DF (saliency as Y)
        ax = fig.add_subplot(sgs[1])
        x_domain_bounds = list(zip(bound_edges, bound_edges[1:]))
        ax.set_title("Bound transform\n to saliency domain")
        viz_bands_on_df(ax, 
                        transformation=input_to_saliency,
                        x_domain_bounds=x_domain_bounds,
                        y_domain_bounds=bounds)
        ax.set_ylabel("saliency_value")
        ax.set_xlabel(bound_gen.domain_name)
        
        # saliency bands visuzalized on DF (area as Y)
        ax = fig.add_subplot(sgs[2])
        ax.set_title("Bound transform\n to area domain")
        viz_bands_on_df(ax, 
                        transformation=lambda x: saliency_to_area(input_to_saliency(x)),
                        x_domain_bounds=x_domain_bounds,
                        y_domain_bounds=area_domain_bounds)
        ax.set_ylabel("area")
        ax.set_xlabel(bound_gen.domain_name)

    viz_banded_masks(fig, 
                     gs_slot=sgs[3], 
                     disp_img=disp_img, 
                     bands=bands, 
                     sample_masks=masks, 
                     partial_smaps=partial_smaps, 
                     n_mask_examples=n_mask_examples, 
                     cmap=cmap)

    ax = fig.add_subplot(sgs[4])
    combined_smap = partial_smaps.sum(dim=0)
    imshow(ax, combined_smap.cpu())
    ax.imshow(disp_img, alpha=0.15)
    ax.set_title("Combined saliency map" + (f"\nIteration {iteration_idx}" if iteration_idx else ""))

    ax = fig.add_subplot(sgs[5])
    viz_del_score_curves(ax, hirise_del_scores, ref_del_scores)
    

def viz_del_score_curves(ax, hirise_del_scores, ref_del_scores, squash_hirise=True, squash_refs=True, hirise_colors=None, ref_color=None):
    if hirise_del_scores.ndim == 1:
        hirise_del_scores = np.expand_dims(hirise_del_scores, axis=0) if isinstance(hirise_del_scores, np.ndarray) else hirise_del_scores.unsqueeze(0)
    
    if ref_del_scores.ndim == 1:
        ref_del_scores = np.expand_dims(ref_del_scores, axis=0) if isinstance(ref_del_scores, np.ndarray) else ref_del_scores.unsqueeze(0)
    
    hirise_colors = hirise_colors or (color_scale(len(hirise_del_scores), cmap="Blues", desc=False, vmin=0.4) if not squash_hirise else 'blue')
    ref_color = ref_color or (color_scale(len(ref_del_scores), cmap="Oranges", desc=False, vmin=0.4) if not squash_refs else 'orange')
    
    x = np.linspace(0, 1, hirise_del_scores.shape[-1])
    for idx, (scores, squash, color, label) in enumerate(zip([hirise_del_scores, ref_del_scores], 
                                                            [squash_hirise, squash_refs],
                                                            [hirise_colors, ref_color],
                                                            ["HiRISE", "RISE"])):
        if squash:
            mean = scores.mean(0) 
            std = scores.std(0) 
            
            ax.plot(x, mean, alpha=0.8, c=color, label=label)
            ax.fill_between(x, mean-std, mean+std, alpha=0.3, color=color)
            ax.legend()
        else:
            for idx, series in enumerate(scores):
                ax.plot(x, series, alpha=0.6, color=color[idx])
                
    ax.set_title("Deletion game history")    
    ax.set_ylabel("confidence score")
    ax.set_xlabel("% of image removed")


def display_bboxes(ax, bboxes, smap, **style_kwargs):
    """
    Draws `bboxes` on `ax`
    * `bboxes` has to be an ndarray of shape (N_bbox, ndim(zyx..), 2(min, max)). Coords have to be normalized to [0,1].
    * smap is used only to scale bboxes
    * `style_kwargs` customizes the drawing style of bboxes (mustbe comaptible with `matplotlib.patches.Rectangle` kwargs)
    """
    _style = {
        **dict(
            linewidth=2,
            edgecolor='r',
            fill=False,
        ),
        **style_kwargs
    }

    for _bbox in bboxes:
        # there might be more than one bbox per img+cls
        _coords_xy = (_bbox * np.atleast_2d(smap.shape).T)[::-1]
        _bbox_anchors = _coords_xy[:, 0]
        _bbox_edges = _coords_xy[:, 1] - _bbox_anchors

        # draw the bbox
        ax.add_patch(
            patches.Rectangle(_bbox_anchors, *_bbox_edges, **_style)
        )


def display_smap_argmax(ax, smap, **style_kwargs):
    """
    Displays the argmax of smap as a single-point scatter plot
    * `style_kwargs` customizes thee style of the point marker (must be compatible with matplotlib.scatter)
    """
    _style = {
        **dict(
            marker='+',
            linewidth=2,
            color='r',
            s=250,
        ),
        **style_kwargs
    }

    # mark the argmax
    max_coords = np.unravel_index(smap.flatten().argmax(), smap.shape)[::-1]
    ax.scatter(*max_coords, **_style)

