import matplotlib
from matplotlib import cm
from matplotlib.axes import Axes
from matplotlib.colors import Colormap

import numpy as np

import shapely.geometry

from typing import *


def color_scale(N, cmap, color_range: Union[Tuple[float], List[float]]) -> Tuple:
    """
    Samples colors from `cmap` from range specifiec by `color_range`

    :param N: number of color samples to pick
    :param cmap: str or colormap object. A Colormap object will be used directly, a str will be used to retrieve a colormap with corresponding name. Will raise if colormap doesn't exist
    :param color_range: a pair of float values, defining the extremes of the sampling range
    :return: list of colors usable by matplotlib
    """
    if len(color_range) != 2:
        raise ValueError(f"color_range must contain exactly 2 values, got {len(color_range)}")

    cmap = cm.get_cmap(cmap) if isinstance(cmap, str) else cmap
    return tuple(map(cmap, np.linspace(*color_range, N)))


def draw_polys(ax: matplotlib.axes.Axes,
               polygons: Sequence[shapely.geometry.Polygon],
               colors: Sequence,
               **kwargs: Dict[str, Any]) -> None:
    """
    Draws `polygons` and fills them with colors specified in `colors`.

    :param ax: axes object to draw the polygons on
    :param polygons: list of polygons to draw
    :param kwargs: passed as-is to ax.fill(), can be used to e.g. specify alpha
    :return: None
    """
    colors, polygons = map(tuple, (colors, polygons))
    if len(polygons) != len(colors):
        raise ValueError(
                f"`polygons` and `colors` must be the same length! Got {len(polygons)} and {len(colors)} respectively")

    for poly, color in zip(polygons, colors):
        ax.fill(*zip(*poly.exterior.coords), color=color, **kwargs)


def draw_polys_monochrome(ax: matplotlib.axes.Axes,
                          polygons: Sequence[shapely.geometry.Polygon],
                          cmap: Union[str, matplotlib.colors.Colormap],
                          color_range: Union[Tuple[float], List[float]] = (0.3, 0.5),
                          **kwargs: Dict[str, Any]) -> None:
    """
    Draws `polygons` using random shades of `cmap` sampled within `color_range`.

    :param ax: axes object to draw the polygons on
    :param polygons: list of polygons to draw
    :param cmap: str or colormap object. A Colormap object will be used directly, a str will be used to retrieve a colormap with corresponding name. Will raise if colormap doesn't exist
    :param color_range: a pair of float values, defining the extremes of the sampling range
    :param kwargs: passed as-is to ax.fill(), can be used to e.g. specify alpha
    :return: None
    """
    colors = color_scale(len(polygons), cmap, color_range)
    draw_polys(ax, polygons, colors, **kwargs)


def draw_points_monochrome(ax: matplotlib.axes.Axes,
                           points: np.ndarray,
                           cmap: Union[str, matplotlib.colors.Colormap, None],
                           color_range: Union[Tuple[float], List[float], None] = (0.3, 0.5),
                           **kwargs: Dict[str, Any]) -> None:
    """
    Draws `polygons` using random shades of `cmap` sampled within `color_range`.

    :param ax: axes object to draw the polygons on
    :param polygons: list of polygons to
    :param cmap: str or colormap object. A Colormap object will be used directly, a str will be used to retrieve a colormap with corresponding name. Will raise if colormap doesn't exist
    :param color_range: a pair of float values, defining the extremes of the sampling range
    :param kwargs: passed as-is to ax.fill(), can be used to e.g. specify alpha
    :return: None
    """

    colors = color_scale(len(points), cmap, color_range)
    ax.scatter(*points.T, color=colors, **kwargs)
