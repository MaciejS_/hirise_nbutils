from typing import Dict, Any, List

import torch
from hirise_tools.iterative.params import v2_paramsets_index, v2_full_param_set, paramset_to_indexing_tuple
from hirise_tools.iterative.persistence import get_maps_archive_type
from hirise_tools.iterative.refs.params import param_sets_from_ref_params, index_from_ref_params


class Selector:
    def __init__(self, data: List, param_order:List[str], paramsets:List, index:Dict) -> None:
        self.data = data
        self.param_order = param_order
        self.paramsets = paramsets
        self.index = index

    def select(self, query: Dict[str, Any]):
        if query is Ellipsis:
            return self.data
        lambda_matching_params = self.select_paramsets(query)
        indexing_tuples_for_matching_params = [paramset_to_indexing_tuple(ps, self.param_order) for ps in lambda_matching_params]
        indices_for_matching_params = [v
                                       for tpl, v
                                       in self.index.items()
                                       if any(tpl == query_tpl for query_tpl in indexing_tuples_for_matching_params)]
        selected_maps = [self.data[idx] for idx in indices_for_matching_params]

        return selected_maps

    def select_paramsets(self, query: Dict[str, Any]):
        if query is Ellipsis:
            return self.paramsets
        return list(filter(lambda ps: Selector.paramset_matches_lambda_query(ps, query), self.paramsets))

    @staticmethod
    def paramset_matches_lambda_query(paramset, query):
        return all(ql(paramset[qk]) for qk, ql in query.items())


class MapsArchiveBrowser:

    @staticmethod
    def from_archive(arx):
        source_archive_type = get_maps_archive_type(arx)

        if source_archive_type == "v1":
            raise NotImplementedError("v1 is not supported yet")
        elif source_archive_type == "v2":
            return V2ArchiveBrowser.from_archive(arx)
        elif source_archive_type == "r1":
            return RefsArchiveBrowser.from_archive(arx)
        else:
            raise RuntimeError(f"\'{source_archive_type}\' is not one of supported archive types")

class V2ArchiveBrowser:

    def __init__(self, results: List[List[torch.Tensor]], params: Dict):
        self.results = results
        self.n_iterations = len(results)
        self.params = params
        self.param_order = params['order']
        self.paramsets = v2_full_param_set(iteration_params=params['iteration'],
                                           variable_params=params['variable'],
                                           fixed_params=params['fixed'],
                                           param_order=params['order'])
        self.index = v2_paramsets_index(iteration_params=params['iteration'],
                                        variable_params=params['variable'],
                                        param_order=params['order'])

        self.iteration_result_selectors = [Selector(data=iteration_results,
                                                    param_order=self.param_order,
                                                    paramsets=iteration_paramsets,
                                                    index=iteration_index)
                                           for iteration_paramsets, iteration_index, iteration_results
                                           in zip(self.paramsets, self.index, self.results)]

    @classmethod
    def from_archive(cls, arx):
        return cls(results=arx['maps'], params=arx['params'])

    def select(self, iteration_lambda_queries):
        return [selector.select(query) if query is not None else None
                for selector, query
                in zip(self.iteration_result_selectors, iteration_lambda_queries)]

    def select_paramsets(self, iteration_lambda_queries):
        return [selector.select_paramsets(query) if query is not None else None
                for selector, query
                in zip(self.iteration_result_selectors, iteration_lambda_queries)]

class RefsArchiveBrowser:

    def __init__(self, results, params):
        self.results = results
        self.params = params
        self.param_order = params['order']
        self.paramsets = param_sets_from_ref_params(param_clusters=params['clusters'],
                                                        param_order=params['order'],
                                                        fixed_params=params['fixed'])
        self.index = index_from_ref_params(param_clusters=params['clusters'],
                                                param_order=params['order'])

        self.selector = Selector(data=self.results,
                                 param_order=self.param_order,
                                 paramsets=self.paramsets,
                                 index=self.index)

    @classmethod
    def from_archive(cls, arx):
        return cls(results=arx['maps'], params=arx['params'])

    def select(self, lambda_query):
        return self.selector.select(lambda_query)

    def select_paramsets(self, lambda_query):
        return self.selector.select_paramsets(lambda_query)

class AltGameArchiveBrowser:

    def __init__(self, results, params, underlying_browser_type):
        self.browsers = {game_type: underlying_browser_type(game_results, params)
                         for game_type, game_results
                         in results.items()}

    @classmethod
    def from_archive(cls, arx):
        prefix, source_archive_type = get_maps_archive_type(arx).split('-', maxsplit=1)

        if not prefix == 'a1':
            raise (f"\'{prefix}\' is not a valid AltGame archive type")

        if source_archive_type == "v1":
            raise NotImplementedError("v1 is not supported yet")
        elif source_archive_type == "v2":
            return cls(arx['scores'], arx['params'], V2ArchiveBrowser)
        elif source_archive_type == "r1":
            return cls(arx['scores'], arx['params'], RefsArchiveBrowser)
        else:
            raise RuntimeError(f"\'{source_archive_type}\' is not one of supported archive types")

    def select(self, lambda_query, game_type):
        if game_type not in self.browsers:
            raise KeyError(f"results for \'{game_type}\' are not available in this archive (got: {set(self.browsers.keys())}) ")

        return self.browsers[game_type].select(lambda_query)

    def select_paramsets(self, lambda_query, game_type):
        if game_type not in self.browsers:
            raise KeyError(f"results for \'{game_type}\' are not available in this archive (got: {set(self.browsers.keys())}) ")

        return self.browsers[game_type].select_paramsets(lambda_query)


    @property
    def game_types(self):
        return self.browsers.keys()