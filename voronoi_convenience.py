from typing import *

import numpy as np
import torch

from hirise.occlusions.gridgen import GridGen
from hirise.voronoi.maskgen import VoronoiMaskGen
from hirise.voronoi.meshgen import *
from hirise.voronoi.renderer import *

from hirise_nbutils.convenience import create_gridgen


def create_meshgen(seed_provider=None):
    """
    seed_coordinate_provider determines the location of mesh "seeds" and therefore, the shape of the mesh

    :return: configured meshgen
    """
    if seed_provider is None:
        seed_provider = UniformRandomSeeds(ndim=2)

    return VoronoiMeshGen(seed_provider, incremental_mesh=True)


def create_VRISE_renderer(fill_polygons:bool=True,
                          outline_style:int=PillowOcclusionRenderer.OUTLINE_HALFTONE,
                          blur_radius_px: int = 4,
                          device:torch.device = torch.device("cpu")) -> OcclusionRenderer:
    """
    Creates a default, recommended configuration of VRISE OcclusionRenderer
    """
    renderer = PillowOcclusionRenderer(device,
                                       fill_polygon=fill_polygons,
                                       outline_type=outline_style)
    blurring_renderer = FixedGaussianBlur(renderer, sigma=blur_radius_px)

    return blurring_renderer


def create_RISE_meshgen():
    """
    Returns a meshgen analogous to a RISE gridgen
    :return:
    """
    seed_provider = CheckerboardPatternSeeds(ndim=2)
    return VoronoiMeshGen(seed_provider, incremental_mesh=False)


def create_RISE_renderer(device:torch.device = torch.device("cpu")):
    """
    Returns a Rednerer configured to emulate the behaviour of RISE - shifting the
    :return:
    """
    renderer = PillowOcclusionRenderer(device,
                                       fill_polygon=True,
                                       outline_type=PillowOcclusionRenderer.OUTLINE_HALFTONE)

    blur = lambda r: OcclusionDependentGaussianBlur(r, min_sigma=1, max_sigma=16)
    shift = lambda r: OcclusionDependentRandomRenderShift(r)

    return blur(shift(renderer))


def create_voronoi_maskgen(meshgen: MeshGen = None,
                           occlusion_selector: GridGen = None,
                           renderer: OcclusionRenderer = None,
                           device=torch.device("cpu")):
    meshgen = meshgen or create_meshgen()
    selector = occlusion_selector or create_gridgen(device=device)
    renderer = renderer or create_VRISE_renderer()
    maskgen = VoronoiMaskGen(mesh_gen=meshgen,
                             occlusion_selector=selector,
                             renderer=renderer)

    return maskgen


def create_RISE_maskgen():

    raise NotImplementedError