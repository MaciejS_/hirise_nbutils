import itertools
import unittest
import numpy as np
import torch

from hirise_nbutils.metrics import adjusted_diff
from parameterized import parameterized


class TestRelativeDiff(unittest.TestCase):

    @parameterized.expand([
        [0, 0, 0],
        [1, 0, 1],
        [0, 1, -1],
        [1, 1, 0],
        *map(list, zip(
            [0.95, 0.85, 0.7525, 0.75, 0.7425, 0.60, 0.45, 0.30],
            itertools.repeat(0.75),
            [0.8, 0.4, 0.01, 0, -0.01, -0.2, -0.4, -0.6]
        )),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.adjdiff({int(100*p.args[-3])}%, {int(100*p.args[-2])}%)={int(100*p.args[-1])}%")
    def test_result(self, x, ref, expected):
        self.assertTrue(np.allclose(adjusted_diff(x, ref), expected))

    @parameterized.expand([
        [np.finfo(float).eps, 0],
        [1 - np.finfo(float).eps, 0],
        [0 + np.finfo(float).eps, 1],
        [1 - np.finfo(float).eps, 1],

        [0, np.finfo(float).eps],
        [1, np.finfo(float).eps],
        [0, 1 - np.finfo(float).eps],
        [1, 1 - np.finfo(float).eps],
    ])
    def test_edge_cases_in_range(self, x, ref):
        result = adjusted_diff(x, ref)
        self.assertTrue(-1 <= result <= 1, msg=f"Expected value in range (-1, 1), got {result}")

    @parameterized.expand([
        *[
            [
                Type([0.95, 0.75, 0.30]),
                Type([0.8, 0.75, 0.6]),
                Type([0.75, 0, -0.5]),
                f"{typename}.1D"
            ]
            for Type, typename
            in zip([np.array, torch.Tensor], ['numpy', 'torch'])
        ],
        *[
            [
                np.array([[0.95, 0.75, 0.30], [0.85, 0.45, 0.7425]]),
                np.array([[0.8, 0.75, 0.6], [0.75, 0.75, 0.75]]),
                np.array([[0.75, 0, -0.5], [0.4, -0.4, -0.01]]),
                f"{typename}.2D"
            ]
            for Type, typename
            in zip([np.array, torch.Tensor], ['numpy', 'torch'])
        ],

    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_multidim(self, x, ref, expected_result, desc):
        result = adjusted_diff(x, ref)

        self.assertTupleEqual(
            result.shape,
            expected_result.shape,
            msg=f"Shapes don't match. Expected {expected_result.shape}, got {result.shape}"
        )
        self.assertTrue(
            np.allclose(result, expected_result),
            msg=f"Result doesn't match. Expected {expected_result}, got {result}"
        )


if __name__ == '__main__':
    unittest.main()
