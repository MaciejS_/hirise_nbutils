from unittest import TestCase, mock

import torch
from parameterized import parameterized

from hirise.evaluation.game import BatchedDeletionGame, BatchedInsertionGame
from hirise_nbutils.convenience import run_alteration_game
from torch import nn


class TestRunAlterationGameConfig(TestCase):

    @parameterized.expand([
        ("del", BatchedDeletionGame, False, "Deletion game"),
        ("ins", BatchedDeletionGame, True, "Insertion game"),
        ("blur", BatchedInsertionGame, True, "Blurring game"),
        ("deblur", BatchedInsertionGame, False, "Deblurring game"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    @mock.patch("hirise_nbutils.convenience.BatchedGameRunner")
    def test_config_of_game_type(self, shortname, expected_type, is_reversed, desc, runner_mock):
        fake_parameters_matrix = torch.zeros([2, 2], dtype=torch.float32, device=torch.device("cpu:0"))
        model_mock = mock.Mock(spec=nn.Module,
                                side_effect=lambda tensor: torch.rand(1000),
                                **{"parameters": lambda: iter([fake_parameters_matrix])})
        # act
        run_alteration_game(game_type=shortname,
                            image=torch.rand(3, 50, 50),
                            explanation=torch.rand(50, 50),
                            model=model_mock)

        runner_mock.return_value.run.assert_called_once()
        captured_game, *_ = runner_mock.return_value.run.call_args[0]

        self.assertIsInstance(captured_game, expected_type)
        self.assertEqual(captured_game.reversed, is_reversed)