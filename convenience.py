from typing import *

import torch
import numpy as np
from hirise.evaluation.game import BatchedDeletionGame, BatchedInsertionGame
from hirise.evaluation.runner import BatchedGameRunner
from hirise.occlusions.maskgen import MaskGen, BandedMaskGen
from hirise.occlusions.gridgen import ThresholdGridGen, SmartCoordinateGridGen, FixingGridGen, GridGen
from hirise.occlusions.upsampling import ParallelizedUpsampler
from hirise.mask_evaluator import MaskEvaluator

from hirise.structures import CombinedMask, VariableFillCombinedMask, LambdaCheckpoints, ArbitraryCheckpoints, \
    CombinedMaskWithCheckpoints

from hirise_tools.checkpoints import npow10
from hirise_tools.models import load_resnet50


def create_gridgen(device: torch.device) -> GridGen:
    """
    Builds a good-enough grid generator with intuitive behaviour
    The returned gridgen is a hybrid with Threshold as base generator and Coordinate as auxilary fixing gen

    :param device: hardware device used to generate grids
    :return:
    """
    tgg = ThresholdGridGen(device)
    cgg = SmartCoordinateGridGen(device)
    return FixingGridGen(grid_gen=tgg, fixer_grid_gen=cgg, fixing_threshold=10 ** -9)


def create_maskgen(device: torch.device, upsampler=None, gridgen=None, pool_size: int = -2) -> MaskGen:
    """
    Builds a default maskgen
    :param device: torch device used to generate masks
    :param gridgen: optional, gridgen implementation to be used instead of the default implementation
    :param pool_size: number of worker threads
    :return:
    """
    upsampler = upsampler or ParallelizedUpsampler(pool_size=pool_size, use_tqdm=False)
    gridgen = gridgen or create_gridgen(device)
    return MaskGen(gridgen, upsampler)


def create_maskgen_banded(device: torch.device, upsampler=None, gridgen=None, pool_size: int = -2) -> BandedMaskGen:
    """
    Builds a default maskgen
    :param device: torch device used to generate masks
    :param gridgen: optional, gridgen implementation to be used instead of the default implementation
    :param pool_size: number of worker threads
    :return:
    """
    upsampler = upsampler or ParallelizedUpsampler(pool_size=pool_size, use_tqdm=False)
    gridgen = gridgen or create_gridgen(device)
    return BandedMaskGen(gridgen, upsampler)


def create_evaluator(
    device: torch.device,
    model=None,
    batch_size: int = 100,
    model_name: str = "ResNet50",
    use_tqdm: bool = False,
    fp16_mode: bool = False,
) -> MaskEvaluator:
    """
    Creates a default MaskEvaluator using ResNet50
    """
    model = model or load_resnet50(device)[0]
    model_name = model_name

    return MaskEvaluator(
        model=model,
        device=device,
        batch_size=batch_size,
        use_tqdm=use_tqdm,
        model_name=model_name,
        use_fp16=fp16_mode
    )


def create_retainer(shape: Tuple[int],
                    expected_p1: Optional[float] = None,
                    chk_indices: Optional[Sequence[int]] = None,
                    _lambda: Optional[Callable] = None) -> CombinedMaskWithCheckpoints:
    """
    Creates a checkpointing MaskAggregator.
    By default (if no optional params are given), the returned retainer will be of LambdaCheckpoints type,
    configured to take a snapshot at each multiple of power of ten.


    :param shape: (Y, X) tuple. Must match the shape of masks fed to this retainer.
    :param expected_p1: If provided, the underlying MaskAggregator will be a CombinedMask, otherwise a VariableFillCombinedMask
    :param chk_indices: overrides the default npow10 checkpointing scheme - snapshots will be taken at mask indices provided by this collection
    :param _lambda: replaces the npow10 scheme with a custom callable that returns True when snapshot should be saved and false otherwise
                    the callable takes three arguments - mask index, mask array, and score and can use any of these values
                    to decide whether create a snapshot or not.
    :return:
    """

    if expected_p1 is None:
        base = VariableFillCombinedMask(shape)
    else:
        base = CombinedMask(shape, expected_p1)

    if chk_indices is not None:
        checkpoints = ArbitraryCheckpoints(base, chk_indices)
    else:
        default_lambda = _lambda or (lambda i, m, s: npow10(i))
        checkpoints = LambdaCheckpoints(base, default_lambda)

    return checkpoints


def run_alteration_game(game_type,
                        image,
                        explanation,
                        model,
                        observed_classes=None,
                        batch_size=200,
                        step_size=224,
                        use_tqdm=False,
                        custom_substrate=None) -> np.array:
    """
    Sets up and runs deletion game for given `image` and `explanation`. Returns confidence scores for all classes at each step
    Refer to documentation of AlterationGame and GameRunner for more information on the parameters.

    Game types:
    * 'del' - pixels are removed from `image`, starting with the most salient. The result in a blank image
    * 'ins' - pixels from `image` are drawn onto a blank image, starting with the most salient
    * 'blur' - pixels from a blurred copy of `image` are copied onto `image`, starting with the most salient
    * 'deblur' - pixels from `image` are copied from a blurred copy of `image`, starting with the most salient ones.

    """

    game_types = {"del":    {"type":     BatchedDeletionGame,
                             "reversed": False},
                  "ins":    {"type":     BatchedDeletionGame,
                             "reversed": True},
                  "blur":   {"type":     BatchedInsertionGame,
                             "reversed": True},
                  "deblur": {"type":     BatchedInsertionGame,
                             "reversed": False},
                  }

    selected_game = game_types[game_type]
    GameType = selected_game["type"]
    game = GameType(model=model,
                     image=image,
                     explanation=explanation,
                     batch_size=batch_size,
                     step_size=step_size,
                     substrate_fn=custom_substrate or GameType.default_substrate(),
                     observed_classes=observed_classes,
                     reverse=selected_game["reversed"])
    return BatchedGameRunner().run(game, leave_bar=False, use_tqdm=use_tqdm)
