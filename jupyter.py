import torch
import numpy as np
from IPython import display

def hide_leftover_progress_bars():
    display.display_javascript(r'''
          var list = document.querySelectorAll('div.lm-Widget.jp-OutputArea-child > div.jupyter-widgets.jp-OutputArea-output.lm-mod-hidden');
          for (var i = 0; i < list.length; i++) {
            list[i].parentNode.classList.add("lm-mod-hidden")
          }''', raw=True)

def reveal_leftover_progress_bars():
      display.display_javascript(r'''
          var list = document.querySelectorAll('div.lm-Widget.jp-OutputArea-child.lm-mod-hidden > div.jupyter-widgets.jp-OutputArea-output.lm-mod-hidden');
          for (var i = 0; i < list.length; i++) {
            list[i].parentNode.classList.remove("lm-mod-hidden")
          }''', raw=True)


def inspect(x):
    if isinstance(x, dict):
        return {inspect(k): inspect(v) for k, v in x.items()}
    if isinstance(x, (list, tuple)):
        return tuple(map(inspect, x))
    elif isinstance(x, torch.Tensor):
        return (x.shape, x.device)
    elif isinstance(x, np.ndarray):
        return (x.shape)
    else:
        return x
    
    
# def inspect_size(x):
#     if isinstance(x, (list, tuple)):
#         container_size, subcontainers = list(map(inspect_size, x))

#         return tuple(sum(map(inspect_size, x)))
#     elif isinstance(x, (torch.Tensor, np.ndarray)):
#         return 1, None
#     else:
#         return 1, None