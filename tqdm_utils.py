from tqdm.autonotebook import tqdm

def tlist(iterable, **kwargs):
    """
    Converts the iterable into a list before passing it to tqdm
    """
    return tqdm(list(iterable), **kwargs)

def tenum(iterable, **kwargs):
    """
    Enumerates the `iterable` before passing it to tqdm
    """
    return tqdm(enumerate(iterable), **kwargs)

def tlenum(iterable, **kwargs):
    """
    Enumerates and converts the interable ito a list before passing it to tqdm
    """
    return tqdm(list(enumerate(iterable)), **kwargs)